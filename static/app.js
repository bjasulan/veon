'use strict';

var app = new Vue({
    el: '#app',
    data: {
        quoteText: '',
        quotesMax: 10,
        errorMessage: '',
        quotes: [
        ],
    },
    created: function() {
        if (this.quotes.length === 0) {
            this.fetchQuotes();
        }
    },
    methods: {
        fetchQuotes: function() {
            this.$http.get('/api/')
            .then(function(res, resp) {
                this.quotes = res.body;
            }, function(res) {
                console.log('fetching error', res);
                this.errorMessage = res;
            });
        },

        saveQuote: function(e) {
            e.preventDefault();
            this.errorMessage = '';

            if (!this.quoteText.length) {
                this.errorMessage = 'The quote cannot be empty';
            }
            else if (this.quotes.length === this.quotesMax) {
                this.errorMessage = 'Для добавления новых цитат удалите одну из добавленных';
                return;
            }
            else {
                this.$http.post('/api/', [{text: this.quoteText}])
                .then(function() {
                    this.quotes.push({text: this.quoteText});
                    this.quoteText = '';
                }, function(res) {
                    console.log('error', res);
                    this.errorMessage = res;
                });
            }
        },

        deleteQuote: function(e) {
            e.preventDefault();
            this.errorMessage = '';
            const id = e.currentTarget.getAttribute('aria-id');

            this.$http.delete('/api/' + id)
            .then(function() {
                this.quotes.splice(Number(id), 1);
            }, function(res) {
                console.log('error', res);
                this.errorMessage = res;
            });
        },
    }
});
