const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const fs = require('fs');
const csp = require('helmet-csp');

const quotePath = path.join(__dirname, '../quotes.json');
const fsOp = {encoding: 'utf-8'};

function updateQuote(quotes, res) {
    fs.writeFile(quotePath, JSON.stringify(quotes), fsOp, err => {
        if (err) throw err;

        res.json({message: 'Success'});
    });
}

module.exports = ({app}) => {
    // app.use(csp({
    //     directives: {defaultSrc: ["'self'"], objectSrc: ["'none'"]},
    // }));
    app.use(express.static('static'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false}));

    app.get('/', (req, res) => {
        res.sendFile(path.join(__dirname, 'views/index.html'));
    });

    const myApi = express.Router();

    app.use('/api', myApi);

    myApi.get('/', (req, res) => {
        fs.readFile(quotePath, fsOp, (err, data) => {
            if (err) throw err;
            res.json(JSON.parse(data));
        });
    });

    myApi.post('/', (req, res) => {
        const quotes = JSON.parse(fs.readFileSync(quotePath, fsOp));
        const text = req.body[0].text;

        if (text && text.length) {
            quotes.push({text});
            updateQuote(quotes, res);
        }
        else {
            res.status(400).send('Quote text cannot be empty');
        }
    });

    myApi.delete('/:quoteId', (req, res) => {
        const quotes = JSON.parse(fs.readFileSync(quotePath, fsOp));
        const quoteId = req.params.quoteId;

        if (!quoteId) {
            return res.status(400).send(':quoteId is empty');
        }

        quotes.splice(Number(quoteId), 1);
        updateQuote(quotes, res);
    });
};
